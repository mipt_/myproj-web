import 'react-app-polyfill/ie9'; // For IE 9-11 support
import 'react-app-polyfill/stable';
// import 'react-app-polyfill/ie11'; // For IE 11 support

import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'mobx-react';
import loadable from '@loadable/component';

import {
  RootStore,
  LStorage,
  API_V1_REQ,
  Loading,
  persist,
  AUTH_KEY,
} from 'Internal';

import './index.scss';

import * as serviceWorker from './serviceWorker';


const App = loadable(() => import('./App'));

const rootStore = RootStore.create(
  {
    authStore: {},
    userStore: {},
    appUIStore: {},
  },
  {
    apiV1: API_V1_REQ,
    localStg: LStorage,
  }
);

API_V1_REQ.interceptors.request.use(
  (config) => {
    config.headers['Authorization'] = `Bearer ${rootStore.authStore.authData.access}`;
    return config;
  },
  (err) => Promise.reject(err),
);

API_V1_REQ.interceptors.response.use(
  (resp) => resp,
  (err) => Promise.reject(err),
);

persist(
  AUTH_KEY,
  rootStore.authStore,
  {
    storage: window.localStorage,
    jsonify: true
  },
  {
    _authData: true,
  }
);

ReactDOM.render(
  <React.StrictMode>
    <Provider stores={rootStore}>
      <App fallback={<Loading/>}/>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
