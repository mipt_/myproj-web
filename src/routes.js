import React from 'react';
import {Redirect} from 'react-router-dom';
import * as R from 'ramda';

import {
  Users,
  UserNew,
  UserEdit,
} from 'Internal';


export function makeRoute(routes) {
  const mr = R.pipe(
    rts => R.map(
      R.cond([
        [R.and(R.startsWith('/'), R.endsWith('/')), r => r.slice(1, -1)],
        [R.startsWith('/'), r => r.substring(1)],
        [R.endsWith('/'), r => r.slice(0, -1)],
        [R.T, r => r]
      ]),
      rts
    ),
    R.join('/'),
    res => R.concat('/', res),
  );
  return mr(routes);
}

export function passRouteParams(routeStr, params) {
  for (const [key, value] of Object.entries(params)) {
    routeStr = routeStr.replace(new RegExp(`:${key}`, 'i'), value);
  }
  return routeStr;
}

export const ROOT_ROUTE = makeRoute([]);
export const MODE_ROUTE = makeRoute([':mode']);
export const PAGE404_ROUTE = makeRoute(['404']);
export const PAGE500_ROUTE = makeRoute(['500']);
export const SIGNIN_ROUTE = makeRoute(['signin']);
export const USERS_ROUTE = makeRoute(['users']);
export const USER_NEW_ROUTE = makeRoute(['user']);
export const USER_EDIT_ROUTE = makeRoute(['user', ':id']);

export const routes = [
  {
    key: 'root-view',
    name: '',
    exact: true,
    path: ROOT_ROUTE,
    render: () => (
      <Redirect to={USERS_ROUTE}/>
    )
  },
  {
    key: 'users-view',
    name: 'Users',
    exact: true,
    path: USERS_ROUTE,
    render: (props) => (
      <Users {...props}/>
    )
  },
  {
    key: 'user-new-view',
    name: 'New user',
    exact: true,
    path: USER_NEW_ROUTE,
    render: (props) => (
      <UserNew {...props}/>
    )
  },
  {
    key: 'user-edit-view',
    name: 'User',
    exact: true,
    path: USER_EDIT_ROUTE,
    render: (props) => (
      <UserEdit {...props}/>
    )
  }
];
