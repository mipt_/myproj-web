import loadable from '@loadable/component';


export const Loading = loadable(() => import('./Loading'));
