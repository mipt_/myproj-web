import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';

import {
  ROOT_ROUTE,
  PAGE404_ROUTE,
  PAGE500_ROUTE,
  SIGNIN_ROUTE,

  DefaultLayout,
  Signin,
  Page404,
  Page500,
} from 'Internal';


function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path={PAGE404_ROUTE} name="Page 404" render={props => (<Page404 {...props}/>)}/>
        <Route exact path={PAGE500_ROUTE} name="Page 500" render={props => (<Page500 {...props}/>)}/>
        <Route exact path={SIGNIN_ROUTE} name="Sign in" render={props => (<Signin {...props}/>)}/>
        <Route path={ROOT_ROUTE} render={props => (<DefaultLayout {...props}/>)}/>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
