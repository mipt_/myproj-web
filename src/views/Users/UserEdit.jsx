import React from 'react';
import {withRouter} from 'react-router-dom';
import {inject, observer} from 'mobx-react';
import * as R from 'ramda';

import {
  ROOT_ROUTE,
} from 'Internal';

import {
  UserForm,
} from './index';


class UserEdit extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      initials: {},
      loading: false,
      errors: {},
    };
  }

  componentDidMount() {
    const {
      match: {
        params
      },
    } = this.props;

    this.getUser(params.id);
  }

  getUser(_id) {
    const {
      stores: {
        userStore,
      },
    } = this.props;

    userStore.getOne(_id).then(
      (resp) => {
        this.setState({initials: resp.data});
      },
      () => {
      },
    );
  }

  handleSubmit = ({id, ...vals}) => {
    const {
      history,
      stores: {
        userStore,
      },
    } = this.props;

    this.setState({loading: true});

    userStore.update(id, vals).then(
      () => history.push(ROOT_ROUTE),
      ({response}) => this.setState({errors: response.data}),
    ).finally(
      () => this.setState({loading: false})
    );
  };

  render() {
    const {
      history,
    } = this.props;

    const {
      initials,
      loading,
      errors,
    } = this.state;

    return (
      <UserForm
        initials={initials}
        onCancel={() => history.push(ROOT_ROUTE)}
        onSubmit={this.handleSubmit}
        loading={loading}
        errors={errors}
      />
    );
  }
}

function applyContext(WrappedComponent) {
  const injectContext = R.pipe(
    observer,
    inject('stores'),
    withRouter
  );
  return injectContext(WrappedComponent);
}

export default applyContext(UserEdit);
