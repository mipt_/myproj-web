import React from 'react';
import {withRouter} from 'react-router-dom';
import {inject, observer} from 'mobx-react';
import * as R from 'ramda';

import {
  USER_EDIT_ROUTE,
  passRouteParams,
} from 'Internal';

import Spinner from 'react-bootstrap/Spinner';


class UserActions extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
    };
  }

  handleRemove = () => {
    const {
      stores: {
        userStore
      },
      data
    } = this.props;

    if (window.confirm(`Delete ${data.username}?`)) {
      this.setState({loading: true});
      userStore.remove(data.id).then(
        () => userStore.getAll(),
        () => {
        }
      ).finally(
        () => this.setState({loading: false})
      );
    }
  };

  render() {
    const {history, data} = this.props;

    const {loading} = this.state;

    return (
      <React.Fragment>
        {loading ? (
          <Spinner animation="border" role="status" size="sm">
            <span className="sr-only">wait...</span>
          </Spinner>
        ) : (
          <React.Fragment>
            <a href="#" onClick={() => history.push(passRouteParams(USER_EDIT_ROUTE, {id: data.id}))}>
              Edit
            </a>
            {' '}
            <a href="#" onClick={this.handleRemove}>
              Delete
            </a>
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

function applyContext(WrappedComponent) {
  const injectContext = R.pipe(
    observer,
    inject('stores'),
    withRouter,
  );
  return injectContext(WrappedComponent);
}

export default applyContext(UserActions);
