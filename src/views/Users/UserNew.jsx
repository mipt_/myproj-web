import React from 'react';
import {withRouter} from 'react-router-dom';
import {inject, observer} from 'mobx-react';
import * as R from 'ramda';

import {
  ROOT_ROUTE,
} from 'Internal';

import {
  UserForm,
} from './index';


class UserNew extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      errors: {},
    };
  }

  handleSubmit = (vals) => {
    const {
      history,
      stores: {
        userStore,
      },
    } = this.props;

    this.setState({loading: true});

    userStore.create(vals).then(
      () => history.push(ROOT_ROUTE),
      ({response}) => this.setState({errors: response.data}),
    ).finally(
      () => this.setState({loading: false})
    );
  };

  render() {
    const {
      history,
    } = this.props;

    const {
      loading,
      errors,
    } = this.state;

    return (
      <UserForm
        initials={{}}
        onCancel={() => history.push(ROOT_ROUTE)}
        onSubmit={this.handleSubmit}
        loading={loading}
        errors={errors}
      />
    );
  }
}

function applyContext(WrappedComponent) {
  const injectContext = R.pipe(
    observer,
    inject('stores'),
    withRouter
  );
  return injectContext(WrappedComponent);
}

export default applyContext(UserNew);
