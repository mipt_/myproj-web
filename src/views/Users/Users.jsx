import React from 'react';
import {withRouter} from 'react-router-dom';
import {inject, observer} from 'mobx-react';
import * as R from 'ramda';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';

import {
  USER_NEW_ROUTE,
} from 'Internal';

import {
  UserActions,
} from './index';


class Users extends React.Component {

  componentDidMount() {
    this.getUsers();
  }

  getUsers() {
    const {
      stores: {
        userStore
      }
    } = this.props;

    userStore.getAll().then(
      () => {
      },
      () => {
      },
    );
  }

  render() {
    const {
      history,
      stores: {
        userStore
      }
    } = this.props;

    return (
      <React.Fragment>
        <Row className="mt-4">
          <Col>
            <Button onClick={() => history.push(USER_NEW_ROUTE)}>
              Add
            </Button>
          </Col>
        </Row>
        <Row>
          <Col>
            <Table className="mt-2" bordered hover striped responsive>
              <thead>
              <tr>
                <th>#</th>
                <th>Username</th>
                <th>Email</th>
                <th>Action</th>
              </tr>
              </thead>
              <tbody>
              {userStore.users.map((user, idx) => (
                <tr key={`user-row-${idx}`}>
                  <td>
                    {idx + 1}
                  </td>
                  <td>
                    {user.username}
                  </td>
                  <td>
                    {user.email}
                  </td>
                  <td>
                    <UserActions data={user}/>
                  </td>
                </tr>
              ))}
              </tbody>
            </Table>
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}

function applyContext(WrappedComponent) {
  const injectContext = R.pipe(
    observer,
    inject('stores'),
    withRouter
  );
  return injectContext(WrappedComponent);
}

export default applyContext(Users);
