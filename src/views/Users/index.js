import React from 'react';
import loadable from '@loadable/component';

import {Loading} from 'Internal';


export const UserActions = loadable(
  () => import('./UserActions'),
  {fallback: (<Loading/>)}
);

export const UserForm = loadable(
  () => import('./UserForm'),
  {fallback: (<Loading/>)}
);

export const UserNew = loadable(
  () => import('./UserNew'),
  {fallback: (<Loading/>)}
);

export const UserEdit = loadable(
  () => import('./UserEdit'),
  {fallback: (<Loading/>)}
);

export const Users = loadable(
  () => import('./Users'),
  {fallback: (<Loading/>)}
);
