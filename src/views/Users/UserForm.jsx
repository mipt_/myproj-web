import React from 'react';
import {withRouter} from 'react-router-dom';
import {inject, observer} from 'mobx-react';
import {createForm} from 'rc-form';
import * as R from 'ramda';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';


class UserForm extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      validated: false,
    };
  }

  handleSubmit = (e) => {
    e.preventDefault();

    const {form, onSubmit} = this.props;

    form.validateFields((errs, vals) => {
      if (!errs) {
        onSubmit(vals);
      }
      this.setState({validated: true});
    });
  };

  render() {
    const {
      form: {
        getFieldDecorator,
        getFieldValue,
        getFieldError,
        validateFields,
      },
      initials,
      errors,
      onCancel,
      loading,
    } = this.props;

    const {
      validated,
    } = this.state;

    getFieldDecorator('id', {
      initialValue: initials.id,
    });

    return (
      <Form className="my-5" onSubmit={this.handleSubmit} noValidate>
        <Form.Group as={Row} controlId="first_name">
          <Form.Label column sm={2}>
            First name
          </Form.Label>
          <Col>
            {getFieldDecorator('first_name', {
              initialValue: initials.first_name,
              rules: [
                {
                  required: false,
                  message: 'Required field',
                },
              ],
            })(<Form.Control
              aria-describedby="first_name_help"
              type="text"
              placeholder=""
              isInvalid={validated && !!(getFieldError('first_name') || errors['first_name'])}
              isValid={validated && !(getFieldError('first_name') || errors['first_name'])}
            />)}
            <Form.Text id="first_name_help" muted>
              Optional.
            </Form.Text>
            {(getFieldError('first_name') || []).map((err, idx) => (
              <Form.Control.Feedback type="invalid" key={`first_name-err-${idx}`}>
                {err}
              </Form.Control.Feedback>
            ))}
            {errors['first_name'] && errors['first_name'].map((err, idx) => (
              <Form.Control.Feedback type="invalid" key={`serv-first_name-err-${idx}`}>
                {err}
              </Form.Control.Feedback>
            ))}
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="last_name">
          <Form.Label column sm={2}>
            Last name
          </Form.Label>
          <Col>
            {getFieldDecorator('last_name', {
              initialValue: initials.last_name,
              rules: [
                {
                  required: false,
                  message: 'Required field',
                },
              ],
            })(<Form.Control
              aria-describedby="last_name_help"
              type="text"
              placeholder=""
              isInvalid={validated && !!(getFieldError('last_name') || errors['last_name'])}
              isValid={validated && !(getFieldError('last_name') || errors['last_name'])}
            />)}
            <Form.Text id="last_name_help" muted>
              Optional.
            </Form.Text>
            {(getFieldError('last_name') || []).map((err, idx) => (
              <Form.Control.Feedback type="invalid" key={`last_name-err-${idx}`}>
                {err}
              </Form.Control.Feedback>
            ))}
            {errors['last_name'] && errors['last_name'].map((err, idx) => (
              <Form.Control.Feedback type="invalid" key={`serv-last_name-err-${idx}`}>
                {err}
              </Form.Control.Feedback>
            ))}
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="username">
          <Form.Label column sm={2}>
            Username
          </Form.Label>
          <Col>
            {getFieldDecorator('username', {
              initialValue: initials.username,
              rules: [
                {
                  required: true,
                  message: 'Required field',
                },
              ],
            })(<Form.Control
              aria-describedby="username_help"
              type="text"
              placeholder=""
              isInvalid={validated && !!(getFieldError('username') || errors['username'])}
              isValid={validated && !(getFieldError('username') || errors['username'])}
              required
            />)}
            <Form.Text id="username_help" muted>
              Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.
            </Form.Text>
            {(getFieldError('username') || []).map((err, idx) => (
              <Form.Control.Feedback type="invalid" key={`username-err-${idx}`}>
                {err}
              </Form.Control.Feedback>
            ))}
            {errors['username'] && errors['username'].map((err, idx) => (
              <Form.Control.Feedback type="invalid" key={`serv-username-err-${idx}`}>
                {err}
              </Form.Control.Feedback>
            ))}
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="email">
          <Form.Label column sm={2}>
            Email
          </Form.Label>
          <Col>
            {getFieldDecorator('email', {
              initialValue: initials.email,
              rules: [
                {
                  type: 'email',
                  required: true,
                  message: 'Required field',
                },
              ],
            })(<Form.Control
              type="email"
              placeholder=""
              isInvalid={validated && !!(getFieldError('email') || errors['email'])}
              isValid={validated && !(getFieldError('email') || errors['email'])}
            />)}
            {(getFieldError('email') || []).map((err, idx) => (
              <Form.Control.Feedback type="invalid" key={`email-err-${idx}`}>
                {err}
              </Form.Control.Feedback>
            ))}
            {errors['email'] && errors['email'].map((err, idx) => (
              <Form.Control.Feedback type="invalid" key={`serv-email-err-${idx}`}>
                {err}
              </Form.Control.Feedback>
            ))}
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="new_password1">
          <Form.Label column sm={2}>
            Password
          </Form.Label>
          <Col>
            {getFieldDecorator('new_password1', {
              rules: [
                {
                  required: false,
                  message: 'Required field',
                },
                {
                  validator() {
                    return !!validateFields(['new_password2']);
                  }
                }
              ],
            })(<Form.Control
              aria-describedby="new_password1_help"
              type="password"
              placeholder=""
              isInvalid={validated && !!(getFieldError('new_password1') || errors['new_password1'])}
              isValid={validated && !(getFieldError('new_password1') || errors['new_password1'])}
            />)}
            <Form.Text id="new_password1_help" muted>
              Your password can’t be too similar to your other personal information.
            </Form.Text>
            <Form.Text id="new_password1_help" muted>
              Your password must contain at least 8 characters.
            </Form.Text>
            <Form.Text id="new_password1_help" muted>
              Your password can’t be a commonly used password.
            </Form.Text>
            <Form.Text id="new_password1_help" muted>
              Your password can’t be entirely numeric.
            </Form.Text>
            <Form.Text id="new_password1_help" muted>
              Optional.
            </Form.Text>
            {(getFieldError('new_password1') || []).map((err, idx) => (
              <Form.Control.Feedback type="invalid" key={`new_password1-err-${idx}`}>
                {err}
              </Form.Control.Feedback>
            ))}
            {errors['new_password1'] && errors['new_password1'].map((err, idx) => (
              <Form.Control.Feedback type="invalid" key={`serv-new_password1-err-${idx}`}>
                {err}
              </Form.Control.Feedback>
            ))}
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="new_password2">
          <Form.Label column sm={2}>
            Confirm Password
          </Form.Label>
          <Col>
            {getFieldDecorator('new_password2', {
              rules: [
                {
                  required: false,
                  message: 'Required field',
                },
                {
                  validator(rule, val) {
                    return val ? val === getFieldValue('new_password1') : true;
                  },
                  message: 'Passwords not match',
                }
              ],
            })(<Form.Control
              aria-describedby="new_password2_help"
              type="password"
              placeholder=""
              isInvalid={validated && !!(getFieldError('new_password2') || errors['new_password2'])}
              isValid={validated && !(getFieldError('new_password2') || errors['new_password2'])}
            />)}
            <Form.Text id="new_password2_help" muted>
              Enter the same password as before, for verification.
            </Form.Text>
            <Form.Text id="new_password2_help" muted>
              Optional.
            </Form.Text>
            {(getFieldError('new_password2') || []).map((err, idx) => (
              <Form.Control.Feedback type="invalid" key={`new_password2-err-${idx}`}>
                {err}
              </Form.Control.Feedback>
            ))}
            {errors['new_password2'] && errors['new_password2'].map((err, idx) => (
              <Form.Control.Feedback type="invalid" key={`serv-new_password2-err-${idx}`}>
                {err}
              </Form.Control.Feedback>
            ))}
          </Col>
        </Form.Group>
        <hr/>
        <Row>
          <Col className="d-flex justify-content-between">
            <Button
              variant="secondary"
              onClick={onCancel}
              disabled={loading}
            >
              Cancel
            </Button>
            <Button
              variant="primary"
              type="submit"
              onClick={this.handleSubmit}
              disabled={loading}
            >
              {loading ? 'Saving...' : 'Save'}
            </Button>
          </Col>
        </Row>
      </Form>
    );
  }
}

function applyContext(WrappedComponent) {
  const injectContext = R.pipe(
    observer,
    inject('stores'),
    withRouter,
    createForm({name: 'user-from'})
  );
  return injectContext(WrappedComponent);
}

export default applyContext(UserForm);
