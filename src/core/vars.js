// modes
export const PUBLIC_MODE = 'app';
export const PRIVATE_MODE = 'admin';

// other
export const UNAUTH_ERROR = 401;
export const SERVER_ERROR = 500;
