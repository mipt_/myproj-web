import {getEnv, getSnapshot, types} from 'mobx-state-tree';
import {when} from 'mobx';
import {fromPromise, FULFILLED, REJECTED, PENDING} from 'mobx-utils';

import {
  AUTH_KEY,
  DomainStore,
  AuthModel,
} from 'Internal';


export const AuthStore = DomainStore.named(
  'AuthStore'
).props({
  _authData: types.optional(
    types.late(() => AuthModel),
    {}
  ),
}).actions(self => {

  const {apiV1, localStg} = getEnv(self);
  const serviceUrl = 'token/';

  function signin(_data = {}, _params = {}) {
    const prom = fromPromise(
      apiV1.post(`${serviceUrl}`, _data, {
        params: _params,
      })
    );

    when(
      () => prom.state === PENDING,
      () => {
        prom.case({
          pending: () => {
            self.runInAction(() => {
              self.setErr([]);
            });
          }
        });
      }
    );

    when(
      () => prom.state === REJECTED,
      () => {
        prom.case({
          rejected: (err) => {
            self.setErr([err.response.data.detail]);
          }
        });
      }
    );

    when(
      () => prom.state === FULFILLED,
      () => {
        prom.case({
          fulfilled: (resp) => {
            self.runInAction(() => {
              self._authData = resp.data;
            });
          }
        });
      }
    );

    return prom;
  }

  function signOut(callback) {
    self.runInAction(() => {
      self._authData = {};
    });
    if (callback) {
      callback();
    }
  }

  function afterCreate() {
    localStg.getItem(AUTH_KEY).then(
      (val) => {
        if (!val) {
          localStg.setItem(AUTH_KEY, JSON.stringify({
            _authData: AuthModel.create(),
          }));
        }
      },
      () => {
      },
    );
  }

  return {
    signin,
    signOut,
    afterCreate,
  };
}).views(self => ({
  get authData() {
    return getSnapshot(self._authData);
  }
}));
