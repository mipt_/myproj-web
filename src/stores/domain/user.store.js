import {getEnv, getSnapshot, types} from 'mobx-state-tree';
import {when} from 'mobx';
import {fromPromise, FULFILLED} from 'mobx-utils';

import {
  DomainStore,
  UserModel
} from 'Internal';


export const UserStore = DomainStore.named(
  'UserStore'
).props({
  _users: types.optional(
    types.array(types.late(() => UserModel)),
    []
  )
}).actions(self => {

  const {apiV1} = getEnv(self);
  const serviceUrl = 'user/';

  function getAll(_params = {}) {
    const prom = fromPromise(
      apiV1.get(`${serviceUrl}`, {
        params: _params,
      })
    );

    when(
      () => prom.state === FULFILLED,
      () => {
        prom.case({
          fulfilled: (resp) => {
            self.runInAction(() => {
              self._users = resp.data.results;
            });
          }
        });
      }
    );

    return prom;
  }

  function getOne(_id, _params = {}) {
    const prom = fromPromise(
      apiV1.get(`${serviceUrl}${_id}/`, {
        params: _params,
      })
    );

    return prom;
  }

  function create(data = {}, _params = {}) {
    const prom = fromPromise(
      apiV1.post(`${serviceUrl}`, data, {
        params: _params,
      })
    );

    return prom;
  }

  function update(id, data = {}, _params = {}) {
    const prom = fromPromise(
      apiV1.put(`${serviceUrl}${id}/`, data, {
        params: _params,
      })
    );

    return prom;
  }

  function remove(_id, _params = {}) {
    const prom = fromPromise(
      apiV1.delete(`${serviceUrl}${_id}/`, {
        params: _params,
      })
    );

    return prom;
  }

  return {
    getAll,
    getOne,
    create,
    update,
    remove,
  };
}).views(self => ({
  get users() {
    return getSnapshot(self._users);
  }
}));
