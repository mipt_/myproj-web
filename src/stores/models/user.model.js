import {types} from 'mobx-state-tree';

import {
  BaseModel
} from 'Internal';


export const UserModel = BaseModel.named(
  'UserModel'
).props({
  id: types.maybeNull(types.number),
  username: types.maybeNull(types.string),
  email: types.maybeNull(types.string),
  first_name: types.maybeNull(types.string),
  last_name: types.maybeNull(types.string),
  new_password1: types.maybeNull(types.string),
  new_password2: types.maybeNull(types.string),
});
