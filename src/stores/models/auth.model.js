import {types} from 'mobx-state-tree';

import {
  BaseModel
} from 'Internal';


export const AuthModel = BaseModel.named(
  'AuthModel'
).props({
  access: types.maybeNull(types.string),
  refresh: types.maybeNull(types.string),
});
