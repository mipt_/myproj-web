import {types} from 'mobx-state-tree';

import {
  AuthStore,
  UserStore,
  UIStore,
} from 'Internal';


export const RootStore = types.model(
  'RootStore',
  {
    // domain
    authStore: types.late(() => AuthStore),
    userStore: types.late(() => UserStore),
    // ui
    appUIStore: types.late(() => UIStore),
  }
);
