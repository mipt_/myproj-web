import {getSnapshot, types} from 'mobx-state-tree';

import {BaseStore} from 'Internal';


export const DomainStore = BaseStore.named(
  'DomainStore'
).props({
  _err: types.optional(
    types.array(types.string),
    []
  )
}).actions(self => {

  function setErr(errs) {
    self.runInAction(() => {
      self._err = errs;
    });
  }

  return {
    setErr,
  };
}).views(self => ({
  get err() {
    return getSnapshot(self._err);
  }
}));
