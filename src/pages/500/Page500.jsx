import React from 'react';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';


export default function Page500() {
  return (
    <Container>
      <Row>
        <Col>
          <h1>500</h1>
        </Col>
      </Row>
    </Container>
  );
}
