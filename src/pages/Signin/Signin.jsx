import React from 'react';
import {withRouter} from 'react-router-dom';
import {inject, observer} from 'mobx-react';
import {createForm} from 'rc-form';
import * as R from 'ramda';

import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

import {
  ROOT_ROUTE,
} from 'Internal';

import './Signin.scss';


class Signin extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      validated: false,
      loading: false,
    };
  }

  handleSubmit = (e) => {
    e.preventDefault();

    const {
      history,
      form,
      stores: {
        authStore,
      },
    } = this.props;

    form.validateFields((errs, vals) => {
      if (!errs) {
        this.setState({loading: true});
        authStore.signin(vals).then(
          () => history.push(ROOT_ROUTE),
          () => {
          },
        ).finally(
          () => this.setState({loading: false})
        );
      }
      this.setState({validated: true});
    });
  };

  render() {
    const {
      form: {
        getFieldDecorator,
        getFieldError,
      },
      stores: {
        authStore
      },
    } = this.props;

    const {
      validated,
      loading,
    } = this.state;

    return (
      <Container className="d-flex align-items-center">
        <Form
          className="form-signin"
          onSubmit={this.handleSubmit}
          validated={validated}
          noValidate
        >
          <h1 className="h3 mb-3 font-weight-normal text-center">Please sign in</h1>
          {authStore.err.map((err, idx) => (
            <Form.Text className="text-danger" key={`serv-err-${idx}`}>
              {err}
            </Form.Text>
          ))}
          <Form.Row>
            <Form.Group as={Col} controlId="username">
              <Form.Label>Username</Form.Label>
              {getFieldDecorator('username', {
                rules: [
                  {
                    required: true,
                    message: 'Required field',
                  },
                ],
              })(<Form.Control type="text" placeholder="Enter username" required disabled={loading}/>)}
              {(getFieldError('username') || []).map((err, idx) => (
                <Form.Control.Feedback type="invalid" key={`username-err-${idx}`}>
                  {err}
                </Form.Control.Feedback>
              ))}
            </Form.Group>
          </Form.Row>
          <Form.Row>
            <Form.Group as={Col} controlId="password">
              <Form.Label>Password</Form.Label>
              {getFieldDecorator('password', {
                rules: [
                  {
                    required: true,
                    message: 'Required field',
                  },
                ],
              })(<Form.Control type="password" placeholder="Enter password" required disabled={loading}/>)}
              {(getFieldError('password') || []).map((err, idx) => (
                <Form.Control.Feedback type="invalid" key={`password-err-${idx}`}>
                  {err}
                </Form.Control.Feedback>
              ))}
            </Form.Group>
          </Form.Row>
          <Button
            type="submit"
            size="lg"
            onSubmit={this.handleSubmit}
            disabled={loading}
            block
          >
            Sign in
          </Button>
        </Form>
      </Container>
    );
  }
}

function applyContext(WrappedComponent) {
  const injectContext = R.pipe(
    observer,
    inject('stores'),
    withRouter,
    createForm({name: 'signin-form'}),
  );
  return injectContext(WrappedComponent);
}

export default applyContext(Signin);
