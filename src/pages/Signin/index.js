import React from 'react';
import loadable from '@loadable/component';

import {Loading} from 'Internal';


export const Signin = loadable(
  () => import('./Signin'),
  {fallback: (<Loading/>)}
);
