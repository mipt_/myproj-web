import React from 'react';
import loadable from '@loadable/component';

import {Loading} from 'Internal';


export const DefaultHeader = loadable(
  () => import('./DefaultHeader'),
  {
    fallback: (<Loading/>),
  }
);
