import React from 'react';
import {withRouter, Link} from 'react-router-dom';
import {inject, observer} from 'mobx-react';
import * as R from 'ramda';

import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

import {
  USERS_ROUTE,
  SIGNIN_ROUTE,
} from 'Internal';


class DefaultHeader extends React.Component {

  render() {
    const {
      history,
      stores: {
        authStore,
      },
    } = this.props;

    return (
      <Navbar bg="dark" variant="dark">
        <Navbar.Brand>
          MyProj
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link as={Link} to={USERS_ROUTE}>Users</Nav.Link>
          </Nav>
        </Navbar.Collapse>
        <Navbar.Collapse className="justify-content-end">
          <Navbar.Text>
            {/* eslint-disable-next-line */}
            <a
              href="#"
              onClick={() => authStore.signOut(() => history.push(SIGNIN_ROUTE))}
            >
              Sign out
            </a>
          </Navbar.Text>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

function applyContext(WrappedComponent) {
  const injectContext = R.pipe(
    observer,
    inject('stores'),
    withRouter
  );
  return injectContext(WrappedComponent);
}

export default applyContext(DefaultHeader);
