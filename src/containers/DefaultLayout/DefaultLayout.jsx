import React from 'react';
import {Route, Switch, withRouter} from 'react-router-dom';
import {inject, observer} from 'mobx-react';
import * as R from 'ramda';

import Container from 'react-bootstrap/Container';

import {
  DefaultHeader,
  routes,
} from 'Internal';


class DefaultLayout extends React.Component {

  render() {

    return (
      <React.Fragment>
        <DefaultHeader/>
        <Container>
          <Switch>
            {routes.map(({key, ...props}) => (
              <Route
                {...props}
                key={key}
              />
            ))}
          </Switch>
        </Container>
      </React.Fragment>
    );
  }
}

function applyContext(WrappedComponent) {
  const injectContext = R.pipe(
    observer,
    inject('stores'),
    withRouter
  );
  return injectContext(WrappedComponent);
}

export default applyContext(DefaultLayout);
