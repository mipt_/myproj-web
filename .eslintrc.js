module.exports = {
  "parser": "babel-eslint",
  "env": {
    "browser": true,
    "es6": true,
    "jest": true,
    "node": true,
  },
  "ignorePatterns": [],
  "extends": [
    "eslint:recommended",
    "plugin:react/recommended",
  ],
  "globals": {
    "Atomics": "readonly",
    "SharedArrayBuffer": "readonly"
  },
  "parserOptions": {
    "ecmaFeatures": {
      "jsx": true
    },
    "ecmaVersion": 2018,
    "sourceType": "module"
  },
  "plugins": [
    "react"
  ],
  "rules": {
    "eqeqeq": "off",
    "curly": "error",
    "quotes": ["error", "single"],
    "no-unused-expressions": "off",
    "arrow-body-style": ["error", "as-needed"],
    "arrow-parens": ["off", "as-needed"],
    "arrow-spacing": ["error", {"before": true, "after": true}],
    "constructor-super": "error",
    "no-confusing-arrow": ["error", {"allowParens": true}],
    "no-const-assign": "error",
    "no-duplicate-imports": ["error", {"includeExports": true}],
    "no-this-before-super": "error",
    "no-var": "error",
    "object-shorthand": ["error", "always", {"avoidQuotes": true}],
    "prefer-const": ["error", {
      "destructuring": "any",
      "ignoreReadBeforeAssign": false
    }],
    "prefer-template": "error",
    "rest-spread-spacing": ["error", "never"],
    "template-curly-spacing": ["off", "always"],
    "block-spacing": "error",
    "brace-style": "error",
    // "camelcase": "error",
    "comma-spacing": ["error", {"before": false, "after": true}],
    "comma-style": ["error", "last"],
    "computed-property-spacing": ["error", "never"],
    "func-call-spacing": ["error", "never"],
    "implicit-arrow-linebreak": ["error", "beside"],
    "keyword-spacing": ["error", {"before": true, "after": true}],
    "max-depth": ["error", 4],
    "multiline-ternary": ["error", "always-multiline"],
    "no-lonely-if": "error",
    "object-curly-spacing": ["off", "always"],
    "semi-spacing": "error",
    "semi-style": ["error", "last"],
    "semi": "error",
    "react/display-name": "off",
    // "indent": [2, 2, {"SwitchCase": 1}],
    "indent": ["off"],
    "react/prop-types": "off"
  },
  "settings": {
    "react": {
      "pragma": "React",
      "version": "15.6.1"
    }
  }
};
