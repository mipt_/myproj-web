# Stage 1 - the build process
FROM node:12.17.0 as builder
RUN mkdir /code
WORKDIR /code
COPY package.json /code/
COPY yarn.lock /code/
RUN yarn install

ENV PATH /code/node_modules/.bin:$PATH

COPY . ./
CMD ["yarn", "run", "start:prod"]
EXPOSE 5000
