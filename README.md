# MyProj-Web
competition app

## Table of Contents

* [Requirements](#requirements)
* [Installation](#installation)
* [Available scripts](#available-scripts)

### Requirements

These are required to pre-install:

- Nodejs 10.x
- Yarn 1.22.x
- NVM (optional)

### Installation

Run the following scripts to install:

```bash
cd ./myproj-web
yarn install
```

### Available scripts

#### `yarn start`

Runs the app in the development mode

#### `yarn start:prod`

Runs the app in the production mode

#### `yarn build`

Builds the app for production to the `build` folder

#### `yarn test`

Launches the test runner in the interactive watch mode

#### `yarn lint`

Runs the linter
